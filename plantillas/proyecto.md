---
layout: 2021/post
section: proposals
category: projects
title: TÍTULO-PROYECTO
---

[RESUMEN: Pequeña introducción y motivación de la propuesta.]

-   Web del proyecto: [URL]

### Contacto(s)

-   Nombre: [NOMBRE]
-   Email: [EMAIL]
-   Web personal: [URL]
-   Mastodon (u otras redes sociales libres): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio o GitHub (u otros sitios de código colaborativo): [URL]

## Comentarios

[COMENTARIOS: Cualquier otro comentario relevante para la organización.]

## Preferencias de privacidad

(Si quieres que tu información de contacto sea anónima, mándamos las propuesta mediante los formularios de la web: <https://eslib.re/2021/propuestas/proyectos/>)

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información del proyecto.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información del proyecto.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
